# Jitsi Meet Conference Mapper

## Overview 

The Jitsi Meet Conference Mapper helps phone callers join a Jitsi Meet
conference.

People who join via the web or the Jitsi Meet electron app simply
click on a link such as https://meet.domain.org/RandomConferenceName.

However, a person who calls in via the phone can't easily key in
"RandomConferenceName" into their phone. 

This program helps translate between the long random name and a 6 digit number.

It should fully implement the API spec outlined here:

https://community.jitsi.org/t/tutorial-self-hosted-conference-mapper-api/53901

In other words, if you pass in id=123456 via GET, it returns the name of the
conference. If you pass in conference=JoeWasHere, it will return the id
(technically it always returns both).

The first time you pass in a conference name that has never been entered
before, a random number is generated and stored. After that, the name/number
pair are reused for all requests.

## Twilio

This program also provides one more API call specifically designed for Twilio.

If you use Twilio as your PSTN/SIP connector, then you can use the
Jitsi Meet Conference Mapper to generate the right response.

The typical way to setup Twilio
(https://community.jitsi.org/t/jigasi-and-twilio-cant-connect-to-sip-trunk/21070/14)
works like this:

 * When a conference room is created, a call to the API is made and the 6 digit
   code is created, stored and provided to the Jitsi Meet admin (via the invite
   button) who sends it to the participants.
 * A person receives both the link to the conference and also the number to
   dial in plus the 6 digit code.
 * A person dials your phone number.
 * Twilio prompts them to enter the 6 digit code for the conference room
 * Twilio sends the 6 digit code to this API (with form data: `Digits=123456`)
   and we return a specially crafted XML response directing Twilio to make a
   SIP call with the X-Room-Name header set to the right conference room.

To configure twilio, create a TwiML Bin with the contents:

```
<?xml version="1.0" encoding="UTF-8"?>
<Response>
   	<Gather numDigits="6" action="https://conference.mapper.meet.yourdomain.org/">
        <Say>Please enter the 6 digit room code, followed by the pound key.</Say>
    </Gather>
</Response>

```
## Dial In Numbers

Lastly, the Jitsi Meet app can display your dial in numbers but it has to know
what those numbers are. It provides a javascript setting: `dialInNumbersUrl`
which should return a JSON formatted response indicating what the dial in
numbers are, e.g.:

    {"message":"Dial-In numbers:","numbers":{"US": ["+1-xxx-xxx-xxx"]},"numbersEnabled":true}


This API can respond with such a message if the `DIALIN_NUMBERS` environment
variable is set to something like: `US:+1-123-456-7890,CA:+1-098-765-4321`.

## Jitsi Meet/Jigasi configuration.

Edit your `/etc/jitsi/meet/domain-config.js` file and add:

 * `dialInNumbersUrl` to `https://conference.mapper.meet.yourdomain.org/?dialin_numbers=1`.
 * `dialInConfCodeUrl` to `https://conference.mapper.meet.yourdomain.org/`.

Edit your `/etc/jitsi/jigasi/sip-communicator.properties` to include the line:

    net.java.sip.communicator.impl.protocol.sip.acc1403273890647.JITSI_MEET_ROOM_HEADER_NAME=X-Room-Name

## Installation

Install the pre-requisites:

    apt install python3-bottle python3-json

Copy `jitsimeet-conference-mapper.py` to `/usr/local/bin/jitsimeet-conference-mapper`.

Create a dedicated user, e.g. `jitsimeet-conference-mapper`.

Edit `jitsimeet-conference-mapper.service` to suit your needs.

Copy `jitsimeet-conference-mapper.service` to `/etc/systemd/system/`. 

Start it: `systemctl start jitsimeet-conference-mapper.service` and enable it
`systemctl enable jitsimeet-conference-mapper.service`.

Optionall install nginx and copy nginx.conf to sites-enabled (and edit it).

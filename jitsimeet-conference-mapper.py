#!/usr/bin/env python3

# Jitsi Meet Conference Mapper is a simple API for mapping Jitsi Meet
# conference names with six digit numbers you can easily type in via your phone
# pad.
#
# It also provides an API for Twilio to use that directs Twilio to join the
# right Jitsi Meet channel using the X-Room-Name header.

from bottle import get, run, template, request, response, post
import os, sys, sqlite3, random, json

# Set configuration variables.
database_directory = os.getenv('DATABASE_DIRECTORY') or "./"
database_path = os.path.join(database_directory, "map.db")
jitsimeet_domain = os.getenv('JITSIMEET_DOMAIN') or 'meet.yourdomain.org'
sip_login = os.getenv('SIP_LOGIN') or '1234567890@yourorg.sip.us1.twilio.com'
dialin_numbers=os.getenv('DIALIN_NUMBERS') or ''
port = os.getenv('PORT') or 8181
host = os.getenv('HOST') or 'localhost'

# Setup the database.
con = sqlite3.connect(database_path)
cur = con.cursor()
cur.execute('''CREATE TABLE IF NOT EXISTS map 
               (conference text, digits integer)''')
con.commit()

def main():
    run(host=host, port=port)

# We only have one get route and one post route.
@post('/')
def post():
    digits = None
    
    try:
        digits = request.params.get("Digits")
    except KeyError:
        pass

    if digits:
        # Only twilio sends us digits and Twilio wants an XML response guiding it
        # to make the right SIP call.
        room = ConferenceForDigits(digits)
        templ = """
<Response>
    <Dial answerOnBridge="true">
        <Sip>
            sip:{{ sip_login }}?X-Room-Name={{ room }}
        </Sip>
    </Dial>
</Response>
"""
        response.set_header('Content-Type', 'application/xml')
        return template(templ, sip_login=sip_login, room=ConferenceForDigits(digits))

@get('/')
def get():
    conference = None
    digits = None
    dialin_numbers_request = False

    try:
        conference = request.query["conference"]
    except KeyError:
        pass

    try:
        digits = request.query["id"]
    except KeyError:
        pass

    try:
        dialin_numbers_request = request.query["dialin_numbers"]
    except KeyError:
        pass

    if dialin_numbers_request and dialin_numbers:
        numbers = {}
        dialin_numbers_list = dialin_numbers.split(',')
        for number_entry in dialin_numbers_list:
            pieces = number_entry.split(':')
            country = pieces[0]
            number = pieces[1]
            if not country in numbers:
                numbers[country] = []
            numbers[country].append(number)

        output = {
            "message":"Dial-In numbers:",
            "numbers": numbers,
            "numbersEnabled": True
        }
        response.set_header('Content-Type', 'application/json')
        return json.dumps(output)


    if conference:
        digits = DigitsForConference(conference)
        if not digits:
            digits = random.randint(100000,999999)
            SaveConferenceDigits(conference, digits)

    if digits:
        conference = ConferenceForDigits(digits)

    #print("Digits are {0}".format(digits))
    #print("conference is {0}".format(conference))

    if conference and digits:
        output = {
            "message":"Successfully retrieved conference mapping",
            "id":digits,
            "conference":"{0}@{1}".format(conference, jitsimeet_domain)
        }
        response.set_header('Content-Type', 'application/json')
        return json.dumps(output)


def SaveConferenceDigits(conference, digits):
    cur.execute('''INSERT INTO map 
        VALUES(?, ?)''', (conference, digits) )
    con.commit()

def ConferenceForDigits(digits):
    cur.execute('''SELECT conference FROM map 
        WHERE digits = ?''', (digits,))
    res = cur.fetchone()
    if res:
        return res[0]
    return None

def DigitsForConference(conference):
    cur.execute('''SELECT digits FROM map 
        WHERE conference = ?''', (conference,))
    res = cur.fetchone()
    if res:
        return res[0]
    return None

if __name__ == "__main__":
    sys.exit(main())
